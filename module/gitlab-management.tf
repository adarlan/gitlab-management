terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16"
    }
  }
}

locals {
  source_project_path = "adarlan/gitlab-management"
}

data "gitlab_project_variable" "GITLAB_USER_NAME" {
  project = local.source_project_path
  key     = "GITLAB_USER_NAME"
}

data "gitlab_project_variable" "GITLAB_USER_EMAIL" {
  project = local.source_project_path
  key     = "GITLAB_USER_EMAIL"
}

data "gitlab_project_variable" "GITLAB_USER_WRITE_REPOSITORY_TOKEN" {
  project = local.source_project_path
  key     = "GITLAB_USER_WRITE_REPOSITORY_TOKEN"
}

resource "gitlab_project_variable" "adarlan__gitlab-pipeline-template-for-docker-images__GITLAB_USER_NAME" {
  project           = "adarlan/gitlab-pipeline-template-for-docker-images"
  variable_type     = data.gitlab_project_variable.GITLAB_USER_NAME.variable_type
  key               = data.gitlab_project_variable.GITLAB_USER_NAME.key
  value             = data.gitlab_project_variable.GITLAB_USER_NAME.value
  raw               = data.gitlab_project_variable.GITLAB_USER_NAME.raw
  protected         = data.gitlab_project_variable.GITLAB_USER_NAME.protected
  masked            = data.gitlab_project_variable.GITLAB_USER_NAME.masked
  environment_scope = data.gitlab_project_variable.GITLAB_USER_NAME.environment_scope
}

resource "gitlab_project_variable" "adarlan__gitlab-pipeline-template-for-docker-images__GITLAB_USER_EMAIL" {
  project           = "adarlan/gitlab-pipeline-template-for-docker-images"
  variable_type     = data.gitlab_project_variable.GITLAB_USER_EMAIL.variable_type
  key               = data.gitlab_project_variable.GITLAB_USER_EMAIL.key
  value             = data.gitlab_project_variable.GITLAB_USER_EMAIL.value
  raw               = data.gitlab_project_variable.GITLAB_USER_EMAIL.raw
  protected         = data.gitlab_project_variable.GITLAB_USER_EMAIL.protected
  masked            = data.gitlab_project_variable.GITLAB_USER_EMAIL.masked
  environment_scope = data.gitlab_project_variable.GITLAB_USER_EMAIL.environment_scope
}
